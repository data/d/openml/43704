# OpenML dataset: Ubudehe-Livestock-1

https://www.openml.org/d/43704

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Overview
Identification
COUNTRY
Rwanda
TITLE
Integrated Household Living Conditions Survey 2010-2011
TRANSLATED TITLE
Enqute Intgrale sur les conditions de vie des mnages 2010-2011
STUDY TYPE
Income/Expenditure/Household Survey
SERIES INFORMATION
This is the third in a series of periodic standardized income and expenditure surveys. The Rwanda EICV is conducted with a periodicity of 5 years. The surveys in the series are as follows:
EICV1 2000-2001
EICV2 2005-2006
EICV3 2010-2011
ID NUMBER
RWA-NISR-EICV3-02
Version
VERSION DESCRIPTION
Version 2.0: Final public-use dataset
PRODUCTION DATE
2012-10-19
NOTES
Version 2.0
The date of this version corresponds to the date of NISR approval of the final public-use datasets.
Overview
ABSTRACT
The 2010/11 Integrated Household Living Conditions Survey or EICV3 (Enqute Intgrale sur les Conditions de Vie des Mnages) is the third in the series of surveys which started in 2000/01 and is designed to monitor poverty and living conditions in Rwanda. The survey methodology has changed little over its 10 years, making it ideal for monitoring changes in the country. In 2010/11, for the first time the achieved sample size of 14,308 households in the EICV3 was sufficient to provide estimates which are reliable at the level of the district.
KIND OF DATA
Sample survey data [ssd]
UNITS OF ANALYSIS
For the purposes of this study, the following units of analysis are considered:
-communities
-households
-persons
Scope
NOTES
The scope of survey is defined by the need to evaluate poverty determinants and effects of poverty in various domains. This includes gathering data in specific sectors and examning summary statistics and computed indicators by consumption indicator, gender etc. The survey primarily seeks to compute household consumption aggregates and correlate consumption to the following areas are within the scope and integrated into the survey:

Education (education expenditures): general education, curriculum, vocational training and, higher learning, school-leaving, literacy and apprenticeship.
Health (health expenditures): disability and health problems, general health and preventative vaccination over the past 12 months.
Migration (travel expenditures): rural-urban migration, internal and external migration.
Housing (expenditures on utilities, rent etc.): status of the housing occupancy, services and installations, physical characteristics of the dwelling, access and satisfaction towards basic services.
Economic activity (revenue): unemployment, underemployment and job search, occupation, wage or salaried employment characteristics, VUP Activities, all other activities, domestic work.
Non-agricultural activities (revenue): activity status, formal and informal sector activity.
Agriculture (income and expenditure) : livestock, land and agricultural equipment, details of holding parcels/blocs and agricultural policy changes, crop harvests and use on a large and small scale crop production, harvests and use, transformation (processing) of agricultural products.

In addition to the specific sector information, consumption and/or wealth holding information was collected:

Consumption: Expenditure on non food items, food expenditure, subsistence farming (own consumption) with different recall periods.
Other cash flows : transfers out by household, transfers received by the household, income support programs  other revenues (excluding all incomes accrued from saving), VUP, UBUDEHE  RSSP schemes, other expenditure (excluding expenditures related to any form of saving).
Stock items: credit, durable assets and savings (household assets and liabilities)

TOPICS
Topic    Vocabulary  URI
consumption/consumer behaviour [1.1]    CESSDA  http://www.nesstar.org/rdf/common
economic conditions and indicators [1.2]    CESSDA  http://www.nesstar.org/rdf/common
EDUCATION [6]    CESSDA  http://www.nesstar.org/rdf/common
general health [8.4]    CESSDA  http://www.nesstar.org/rdf/common
employment [3.1]    CESSDA  http://www.nesstar.org/rdf/common
unemployment [3.5]    CESSDA  http://www.nesstar.org/rdf/common
housing [10.1]    CESSDA  http://www.nesstar.org/rdf/common
time use [13.9]    CESSDA  http://www.nesstar.org/rdf/common
migration [14.3]    CESSDA  http://www.nesstar.org/rdf/common
information technology [16.2]    CESSDA  http://www.nesstar.org/rdf/common
Coverage
GEOGRAPHIC COVERAGE
This is a national survey with representivity at the (5) provicial and (30) district level and includes urban and rural households.
GEOGRAPHIC UNIT
The cluster
UNIVERSE
All household members.
Producers and Sponsors
PRIMARY INVESTIGATOR(S)
Name    Affiliation
National Institute of Statistics of Rwanda (NISR)    Ministry of finance and economics planning (MINECOFIN)
OTHER PRODUCER(S)
Name    Affiliation Role
Oxford Policy Management    DFID    Permanante assistance
Geoffrey Greenwell    UNDP    Designer of data system
David Megill    UNDP    Statistician
Metadata Production
METADATA PRODUCED BY
Name    Abbreviation    Affiliation Role
Juste NITIEMA        Oxford Policy Management (OPM)  Developed the document
Geoffrey Greenwell        UNDP    Reviewed and edited document
Ruben Muhayiteto        NISR    Revision of metadata
DATE OF METADATA PRODUCTION
2011-06-02
DDI DOCUMENT VERSION
Version 1.0 (Oct. 19,2012) 
This version of the document represents the first draft of the public-use dataset of the EICV 3 study.
Version 1.1 (June 28th ,2016): Changed the title from French into English
DDI DOCUMENT ID
RWA-NISR-DDI-EICV3-02

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43704) of an [OpenML dataset](https://www.openml.org/d/43704). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43704/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43704/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43704/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

